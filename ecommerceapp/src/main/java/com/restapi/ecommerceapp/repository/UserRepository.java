package com.restapi.ecommerceapp.repository;

import com.restapi.ecommerceapp.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, String> {

}
