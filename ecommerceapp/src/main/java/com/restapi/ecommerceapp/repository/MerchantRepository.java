package com.restapi.ecommerceapp.repository;

import com.restapi.ecommerceapp.entity.Merchant;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MerchantRepository extends JpaRepository<Merchant, String> {

}
