package com.restapi.ecommerceapp.repository;

import com.restapi.ecommerceapp.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category, String> {

}
