package com.restapi.ecommerceapp.controller;

import com.restapi.ecommerceapp.entity.Merchant;
import com.restapi.ecommerceapp.service.MerchantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/merchant")
public class MerchantController {

    @Autowired
    MerchantService merchantService;


    @PostMapping("/")
    public ResponseEntity<Object> create(@RequestBody Merchant merchant){
        return merchantService.create(merchant);
    }

    @GetMapping("/merchants")
    public ResponseEntity<Object> getMerchant(){
        return merchantService.getAllMerchant();
    }


}
