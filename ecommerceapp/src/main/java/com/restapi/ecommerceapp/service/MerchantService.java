package com.restapi.ecommerceapp.service;

import com.restapi.ecommerceapp.entity.Merchant;
import com.restapi.ecommerceapp.exception.ResponHandler;
import com.restapi.ecommerceapp.repository.MerchantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.UUID;

@Service
public class MerchantService {

    @Autowired
    MerchantRepository merchantRepository;

    public ResponseEntity<Object> create(Merchant merchant) {
        try {
            merchant.setId(UUID.randomUUID().toString());
            var res = merchantRepository.save(merchant);
            return ResponHandler.generateResponse(res.getMerchant_name(), "Success", HttpStatus.OK);
        } catch (Exception e) {
            return ResponHandler.generateResponse(e.getMessage(), "Internal Server Error",
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<Object> getAllMerchant() {
        try {
            List<Merchant> res = merchantRepository.findAll();
            return ResponHandler.generateResponse(res, "Get merchant success", HttpStatus.OK);
        } catch (Exception e) {
            return ResponHandler.generateResponse(e.getMessage(), "Internal Server Error",
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
