package com.restapi.ecommerceapp.service;

import com.restapi.ecommerceapp.entity.User;
import com.restapi.ecommerceapp.exception.ResponHandler;
import com.restapi.ecommerceapp.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import java.util.UUID;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public ResponseEntity<Object> create(User user){
        try {
            user.setId(UUID.randomUUID().toString());
            var res = userRepository.save(user);
            return ResponHandler.generateResponse(res.getEmail(), "Success", HttpStatus.OK);
        } catch (Exception e) {
            return ResponHandler.generateResponse(e.getMessage(), "Internal Server Error",
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
