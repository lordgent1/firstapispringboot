package com.restapi.ecommerceapp.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.Map;

public class ResponHandler {
    public static ResponseEntity<Object> generateResponse(Object responseObj, String message, HttpStatus status) {
        Map<String, Object> map = new HashMap<>();
        map.put("data", responseObj);
        map.put("status", status.value());
        map.put("message", message);
        return new ResponseEntity<>(map, status);
    }

}
