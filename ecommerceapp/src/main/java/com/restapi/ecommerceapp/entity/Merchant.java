package com.restapi.ecommerceapp.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.Data;

import java.io.Serializable;

@Data
@Entity
public class Merchant implements Serializable {

    @Id
    private String id;
    private String merchant_name;
    private String marchant_phone;
    private String address;

    @ManyToOne
    @JoinColumn(name = "userid")
    private User userid;

    public Merchant() {
    }

    public Merchant(String id, String merchant_name, String marchant_phone, String address, User userid) {
        this.id = id;
        this.merchant_name = merchant_name;
        this.marchant_phone = marchant_phone;
        this.address = address;
        this.userid = userid;
    }
}
