package com.restapi.ecommerceapp.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.Data;

import java.io.Serializable;

@Entity
@Data
public class Product implements Serializable {

    @Id
    private String id;
    private String name;
    private String price;
    private String description;
    private String photo;
    @JoinColumn(name = "idmerchant")
    @ManyToOne
    private Merchant idmerchant;




}
