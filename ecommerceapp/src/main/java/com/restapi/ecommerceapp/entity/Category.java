package com.restapi.ecommerceapp.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Data;

import java.io.Serializable;

@Data
@Entity
public class Category implements Serializable {
    @Id
    private String id;
    private String name;

    public Category(){

    }

    public Category(String id, String name) {
        this.id = id;
        this.name = name;
    }
}
