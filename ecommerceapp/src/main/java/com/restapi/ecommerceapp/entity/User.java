package com.restapi.ecommerceapp.entity;


import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Data;

import java.io.Serializable;

@Data
@Entity
public class User implements Serializable {
    @Id
    private  String id;
    private String email;
    private  String password;

    public User(){

    }

    public User(String id, String email, String password) {
        this.id = id;
        this.email = email;
        this.password = password;
    }
}
