package com.restapi.ecommerceapp.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.Data;

import java.io.Serializable;

@Entity
@Data
public class Customer implements Serializable {

    @Id
    private String id;
    private String name;
    private String phonenumber;
    private String address;
    @ManyToOne
    @JoinColumn(name = "userid")
    private User userid;
    private String nik;

    public Customer() {

    }

    public Customer(String id, String name, String phonenumber, String address, User userid, String nik) {
        this.id = id;
        this.name = name;
        this.phonenumber = phonenumber;
        this.address = address;
        this.userid = userid;
        this.nik = nik;
    }
}
